﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * https://docs.microsoft.com/pt-br/dotnet/csharp/programming-guide/classes-and-structs/properties
 * Interfaces tbm possuem propriedades
 * https://docs.microsoft.com/pt-br/dotnet/csharp/programming-guide/classes-and-structs/interface-properties
 */

namespace XplorerProperties
{

    public class Quadrilateral3D
    {
        // autoimplementadas
        public int d {get; private set;}
        public int w {get; private set;}
        public int h {get; private set;}
        public Quadrilateral3D(int w, int h, int d)
        {
            this.w = w;
            this.h = h;
            this.d = d;
        }
        // property expression, somente leitura
        public string Dimension => $"{w}:{h}:{d}";
    }

    // c# 7
    public class AnotherQ3D
    {
        private int w, h, d;
        public AnotherQ3D(int w, int h, int d)
        {
            W = w;
            H = h;
            D = d;
        }

        public int D
        {
            get => d;
            private set => d = value;
        }

        public int W
        {
            get => w;
            private set => w = value;
        }

        public int H
        {
            get => h;
            private set => h = value;
        }

        public string Dimension => $"{W}:{H}:{D}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            int W = 10;
            int H = 20;
            int D = 30;
            var quad = new Quadrilateral3D(W, H, D);
            Console.WriteLine(quad.Dimension);
            var anotherQuad = new AnotherQ3D(12, 25, 30);
            Console.WriteLine(anotherQuad.Dimension);
            Console.ReadKey();
        }
    }
}
