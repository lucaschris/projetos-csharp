﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functional
{
    public static class ExtesionMethodIEnumerable
    {
        // The @ symbol allows you to escape identifiers within your code.
        // @this @static @bool
        public static void Print<T>(this IEnumerable<T> Ie)
        {
            foreach (T item in Ie)
            {
                Console.WriteLine(item);
            }
        }

        public static void Change<Params, Result>(this IEnumerable<Params> Ie
            , Func<Params, Result> Fn)
        {
            foreach(Params t in Ie)
            {
                Fn(t);
            }
        }
    }

    class Program
    {
        static void TestLambdaAndLinq()
        {
            List<int> list = new List<int>();
            for(int i=0; i<1000; i++)
                list.Add(i);
            // lambdas ou funcoes anonimas _ => _ % 8 == 0
            IEnumerable<int> Ie = list.Where(_=>_ % 8 == 0)
                .OrderByDescending(_=>_)
                .Take(100);
            Console.WriteLine(Ie.Count());
            // Linq
            var f = (from c in list
                     where c % 8 == 0
                     select c).Take(100);
            Console.WriteLine(f.Count());
        }

        public delegate IEnumerable<T> FilterModN<T>(List<T> list, int mod);

        static void TestDelegateFunction()
        {
            List<int> list = new List<int>();
            for (int i = 0; i < 1000; i++)
                list.Add(i);
            FilterModN<int> Fn = FilterMod;
            Console.WriteLine((Fn(list, 8)).Count());
        }

        static void TestFunc()
        {
            List<int> list = new List<int>();
            for (int i = 0; i < 1000; i++)
                list.Add(i);
            // Testando Function: https://msdn.microsoft.com/en-us/library/bb549151(v=vs.110).aspx
            // Func<params in T, out TResult>
            Func<List<int>, int, IEnumerable<int>> TFilter = FilterMod;
            TFilter(list, 8).Print();
        }

        static IEnumerable<int> FilterMod(List<int> list, int mod)
        {
            return list.Where(_=>_ % 8 == 0);
        }
    }
}
