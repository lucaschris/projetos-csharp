﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functional
{
    class Anonymous
    {
        static void TestAnonymousFunc()
        {
            // Func<params T, TResult>
            Func<List<String>, IEnumerable<String>> ToUpperFirstLetter = delegate (List<String> list)
            {
                return list.Select(item => {
                    int Length = item.Length;
                    string firstletter = item.Substring(0, 1);
                    string rest = item.Substring(1, Length-1);
                    return string.Format("{0}{1}", firstletter.ToUpper(), rest);
                }
                );
            };

            List<String> names = new List<string>();
            names.Add("christoffer");
            names.Add("Lucas");
            names.Add("fernandes");
            names.Add("santos");

            // Print é um metodo de estacao definido no Arquivo Program.cs
            ToUpperFirstLetter(names).Print();
        }

        static void TestAnonFunc2()
        {
            Action<List<int>> FnAction = delegate (List<int> list)
            {
                //list.ForEach(delegate (int i) { i *= i; });
                list.Change(i => fn(i));
            };

            List<int> numbers = new List<int>();
            numbers.Add(3);
            numbers.Add(4);
            numbers.Add(5);
            numbers.Add(6);
            FnAction(numbers);
            ((IEnumerable<int>)numbers).Print();
        }

        static int fn(int b)
        {
            return b * b;
        }
    }
}
