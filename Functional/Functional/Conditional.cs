﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functional
{
    class Conditional
    {

        public static bool Div(int a, int b)
        {
            return a % b == 0;
        }

        public static bool Div(Func<int, int, bool> Fn)
        {
            return false;
        }

        static Predicate<Func<int, int, bool>> G()
        {
            return delegate(Func<int, int, bool> Fx) {
               Fx = Div;
               return Fx(0, 1);
            };

        }

        static void T()
        {
            int[] array = { 11, 121, 1235, 15456 };
            /***
             * Array.Find retorna o primeiro elemento que corresponde
             * com a funcao condicional passada por parametro.
             * Se nenhum elemento for encontrado o valor padrao
             */
            int r = Array.Find<int>(array, i => Div(i, 11));
            Console.WriteLine(r);

        }

        static void Main(string[] Args)
        {
            T();
        }
    }
}
