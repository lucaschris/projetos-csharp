﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

/**
 *  * Delegate:
 * // https://msdn.microsoft.com/pt-br/library/bb549151(v=vs.110).aspx
 * 
 * Encapsula um metodo com um retorno TResult com N parametros
 * O metodo que sera encapsulado deve ser estatico
 * 
 * A forma de criar um metodo delegate e assim
 * public delegate Retorno NomeDoMetodo<Retorno, T>(T arg1, T arg2)
 * 
 * Action:
 * 
 * Junto com Func<T> foi adicionado a partir do c# 3.0 para que o programador
 * nao precise mais declarar delegate de forma explicita.
 * 
 * Permite criar uma funcao anonima que nao retorna valor, so executa um processo
 * sobre os argumentos passados a ela
 * 
 * Action<T arg1, T arg2, T arg3>
 * 
 * Func:
 * 
 * Permite criar uma funcao com N argumentos que retorna um valor
 * Func<T arg1, T arg2, T arg3, T argN, TResult result>
 * 
 * Lambda Expression
 * 
 * Func<T, TResult> Fx = (T args) => () { return N }
 * Func<T, TResult> Fx = (T args) => N
 * Func<TResult> Fx = () => N
 * 
 * 
 * 
 * Anonymous Function:
 * 
 * Com o uso da palavra chave delegate podemos criar funcoes denominadas
 * de anonimas.
 * 
 *   public delegate void Println(string format, params object[] args);
 *   Println p = delegate(string format, params object[] args) { Console.WriteLine(format, args) }
 * 
 * 
 */


namespace XploreFuncAndActionKeywords
{
    // Criando um delegate com retorno com tipo definido
    public delegate int DelegateBinarySearch<T>(IList<T> list, T target);
    // Criando um delegate com retorno generico
    public delegate TResult Fn<TResult>(int param);
    // Criando um delegate com multiplos parametros
    public delegate void Println(string format, params object[] args);
    //
    public delegate void G(int a, int b, int c, int d, params int [] args);

    static class ExtensionIList {

        public static int BSearch<T>(this IList<T> list, T target) {
            int lf = 0, ri = list.Count;
            while(lf <= ri)
            {
                int mid = (ri - lf) / 2 + lf;
                IComparable<T> comp = list[mid] as IComparable<T>;
                if (comp.CompareTo(target) == 0)
                    return mid;
                else if (comp.CompareTo(target) > 0)
                    ri = mid - 1;
                else
                    lf = mid + 1;
            }
            return -1;
        }
    }

    class Program
    {
        public static int BSearch<T>(IList<T> list, T target)
        {
            int lf = 0, ri = list.Count;
            while (lf <= ri)
            {
                int mid = (ri - lf) / 2 + lf;
                IComparable<T> comp = list[mid] as IComparable<T>;
                if (comp.CompareTo(target) == 0)
                    return mid;
                else if (comp.CompareTo(target) > 0)
                    ri = mid - 1;
                else
                    lf = mid + 1;
            }
            return -1;
        }

        static Func<int[], int, int> BinarySearch = (int[] array, int target) => {
            int l = 0, r = array.Count() - 1;
            while (l <= r)
            {
                int m = (r - l) / 2 + l;
                if (array[m] == target)
                    return m;
                else if (array[m] > target)
                    r = m - 1;
                else
                    l = m + 1;
            }
            return -1;
        };

        static void RunTestFunc()
        {
            OverPrintln(BinarySearch.Invoke(new int[] { 1, 2, 3, 4, 5, 6 }, 7));
        }

        static void RunTestExtensionBSearchOnIList()
        {
            List<int> list = new List<int>();
            list.Add(10);
            list.Add(20);
            list.Add(30);
            list.Add(100);
            list.Add(200);
            OverPrintln(list.BSearch<int>(200));
        }

        static void RunTestAddFunctionToDelegate()
        {
            DelegateBinarySearch<int> Bs = BSearch;
            List<int> list = new List<int>();
            list.Add(10);
            list.Add(20);
            list.Add(30);
            list.Add(100);
            list.Add(200);
            OverPrintln(Bs(list, 100));
        }

        /**
         * Para criar uma funcao anonima que nao retorna valor
         * usamos a Action
         */
        static Action<object> OverPrintln = delegate (object data)
        {
            Console.WriteLine(data);
        };

        //
        static Action<object> PrintLine = (object data) => Console.WriteLine(data);
        //
        // leitura de inteiro
        static Func<int> ReadInt = () => int.Parse(ReadLine());
        // leitura de inteiros
        static Func<char, int[]> ReadInts = (char del) => ReadLine().Split(del).Select(x => int.Parse(x)).ToArray();
        // ReadLineString
        static Func<string> ReadLine = () => Console.ReadLine();

        static Action ReadEndOfLine = () =>
        {
            string str = null;
            while ((str = ReadLine()) != null)
            {


            }
        };

        static Action AnyAction         = () => Console.WriteLine(0xff & 0xf1);
        static Action AnyAction2        = () => {};

        static Println println = delegate (string format, object[] args) { Console.WriteLine(format, args); };

        public static void RunTestLambdaDelegate()
        {
            /**
             * https://pt.wikipedia.org/wiki/Conjectura_de_Collatz
             */
            Func<int, int> Collatz = delegate(int n) {
                int counter = 0;
                while (n != 1)
                {
                    n = (n & 1) == 0 ? n/2 : 3 * n + 1;
                    counter++;
                }
                return counter;
            };
            //Collatz.Invoke(30);
            var rs = Collatz(30);
            OverPrintln( $"Resultado: ({rs})");
            Fn<int> fn = delegate (int x)
            {
                return Collatz.Invoke(x);
            };
            OverPrintln($"Generic Delegate: ({fn(30)})");
        }

        public static Func<int> G = () => 2 * 3;
        public static Func<int, int, int> GDC = (int a, int b) => a % b == 0 ? b : GDC(b, a % b);
        public static Func<Func<int, int, int>> R = () => GDC;

        static void T(string[] args)
        {
            RunTestLambdaDelegate();
            AnyAction();
            println("{0}", R()(56, 16));
        }
    }
}
