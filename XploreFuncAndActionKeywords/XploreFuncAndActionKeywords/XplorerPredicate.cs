﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XploreFuncAndActionKeywords
{
    class XplorerPredicate
    {
        // static Action<int, int> multiply = (int a, int b) => Console.WriteLine(a * b); 
        static Predicate<int> IsPrime = (int num) =>
        {
            return true;
        };

        static Predicate<string> IsPasswordStrong = s =>
        {
            return true;
        };

        public static void Main(string[] args)
        {
            Console.WriteLine();
        }
    }
}
