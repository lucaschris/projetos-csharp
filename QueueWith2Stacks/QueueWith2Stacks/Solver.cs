﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueWith2Stacks
{
    class Solver
    {
        public static int[] stack, copyStack;
        public static int back = 0, front = 0;
        public static string reader()
        {
            return Console.ReadLine().Trim(' ');
        }

        public static int readInt()
        {
            return int.Parse(reader());
        }

        public static int[] readInts()
        {
            string[] str = reader().Split(' ');
            int[] array = new int[str.Length];
            for (int idx = 0; idx < array.Length; idx++)
            {
                array[idx] = int.Parse(str[idx]);
            }
            return array;
        }

        static void enqueue(int value)
        {
            stack[back]     = value;
            copyStack[back] = value;
            back++;
        }

        static int dequeue()
        {
            if (front + 1 > back)
            {
                copyStack[front] = -1;
                return -1; ;
            }
            copyStack[front] = 0;
            return copyStack[front++];
        }

        static int peek()
        {
            return stack[front];
        }

        public static void run()
        {
            int cases = readInt();
            stack = new int[cases];
            copyStack = new int[cases];

            while (cases > 0)
            {
                int[] array = readInts();
                if (array[0] == 1)
                {
                    enqueue(array[1]);
                }
                else if (array[0] == 2)
                {
                    dequeue();
                }
                else
                {
                    Console.WriteLine(peek());
                }
                cases--;
            }
        }

        static void Main(string[] args)
        {
            run();
            foreach (int i in copyStack)
                if (i > 0)
                    Console.WriteLine(i);
        }
    }
}
