﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueWith2Stacks
{
    class Program
    {
        public static int[] stack;
        public static int back = 0, front = 0;

        public static string reader()
        {
            return Console.ReadLine().Trim(' ');
        }

        public static int readInt()
        {
            return int.Parse(reader());
        }

        public static int [] readInts()
        {
            string [] str = reader().Split(' ');
            int [] array = new int[str.Length];
            for(int idx=0; idx<array.Length; idx++)
            {
                array[idx] = int.Parse(str[idx]);
            }
            return array;
        }

        public static void enqueue(int data)
        {
            stack[back++] = data;
        }

        public static int peek()
        {
            return front < 0 ? -2 : stack[front];
        }

        static int dequeue()
        {
            if (front+1 > back)
            {
                stack[front] = -1;
                return -1; ;
            }
            stack[front] = 0;
            return stack[front++];
        }

        static void solver1()
        {
            int cases = readInt();
            stack = new int[cases];
            while (cases > 0)
            {
                int[] array = readInts();
                if (array[0] == 1)
                {
                    enqueue(array[1]);
                }
                else if (array[0] == 2)
                {
                    dequeue();
                }
                else
                {
                    Console.WriteLine(peek());
                }
                cases--;
            }
            foreach (int i in stack)
                if (i > 0)
                    Console.WriteLine(i);
        }
    }
}
