﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Net.Http;
using System.Net;

using System.IO;


/**
 * https://docs.microsoft.com/pt-br/dotnet/csharp/async
 */
namespace XploreAsyncAwait
{
    class ExecuteListAsyncTask
    {
        /*
        private static Func<Bitmap> X()
        {
            return X;
        }
        */

        public async void SomeWhere()
        {
            try
            {
                //string path = "C:\\Users\\r028367\\Desktop"; 
                // Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                string[] str = { "um", "arquivo", "maroto" };
                StreamWriter sw = new StreamWriter("teste.txt");
                foreach (string s in str)
                    await sw.WriteLineAsync(s);
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return;
        }

        public void RunSimpleExampleAsyncTask()
        {
            Task task = new Task(action: SomeWhere);
            task.Start();
            Console.WriteLine("Start AsyncTask");
        }

        public async Task<string> GetWebPage()
        {
            string url = "http://en.wikipedia.org/";
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(url);
            HttpContent httpContent = httpResponseMessage.Content;
            string result = await httpContent.ReadAsStringAsync();
            return result;
        }

        public async void Run()
        {
            var r = await GetWebPage();
            StreamWriter sw = new StreamWriter("raw\\teste.txt");
            sw.WriteLine(r);
        }

        public void RunGetWebPageAsync()
        {
            Task T = new Task(Run);
            T.Start();
        }

        static void Main(string [] args)
        {
            var v = new ExecuteListAsyncTask();
            v.RunGetWebPageAsync();
            Console.WriteLine("END");
        }
    }
}
