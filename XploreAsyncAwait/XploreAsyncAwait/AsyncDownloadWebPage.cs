﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;

using System.Runtime.CompilerServices;
using System.IO;


/**
 * https://docs.microsoft.com/pt-br/dotnet/csharp/async
 * 
 */

namespace XploreAsyncAwait
{
    public static class ExtensionMemoryStream
    {
        public static string ReadToEnd(this MemoryStream ms)
        {
            ms.Position = 0;
            StreamReader st = new StreamReader(ms);
            return st.ReadToEnd();
        }
    }

    class AsyncDownloadWebPage
    {
        private HttpClient httpClient = new HttpClient();

        // palavra chave asyn torna a execucao do metodo assincrona
        public async Task<string> GetStringHTML()
        {
            // a palavra chave await suspende o metodo que esta sendo executado
            // retornando para o metodo que o chamador. Quando o processo
            // em plano de fundo terminar o metodo suspenso volta a executar
            var data = await httpClient.GetStringAsync("http://dotnetfoundation.org");
            // await so pode ser usado dentro de um metodo assincrono
            return data;
        }

        public async Task<Stream> GetStreamHTML()
        {
            var data = await httpClient.GetStreamAsync("http://dotnetfoundation.org");
            return data;
        }

        public void run1()
        {
            TaskAwaiter<string> taskAwaiterWrapperString = GetStringHTML().GetAwaiter();
            string result = taskAwaiterWrapperString.GetResult();
            Console.WriteLine($"Resultado requisicao assincrona.\n{result}");
        }


        public static string MemoryStreamToString(MemoryStream ms)
        {
            ms.Position = 0;
            StreamReader st = new StreamReader(ms);
            return st.ReadToEnd();
        }

        public void run2()
        {
            TaskAwaiter<Stream> taskWaitterWrapperStream = GetStreamHTML().GetAwaiter();
            Stream stream = taskWaitterWrapperStream.GetResult();
            MemoryStream ms = stream.ReaderStream();
            Console.WriteLine(ms.ReadToEnd());
        }

        public void ReaderStream(Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                int sizeBuffer = 1 << 16;
                byte[] buffer = new byte[sizeBuffer];
                int readed;
                int c = 1;
                while ((readed = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, readed);
                    c++;
                }
                Console.WriteLine(c);
                Console.WriteLine("Stream com {0} bytes.\n{1}"
                    , c * sizeBuffer, memoryStream.ReadToEnd());
            }
        }

        static void Test(string[] args)
        {
            AsyncDownloadWebPage p = new AsyncDownloadWebPage();
            p.run2();
            Console.WriteLine("FIM");
        }
    }
}
