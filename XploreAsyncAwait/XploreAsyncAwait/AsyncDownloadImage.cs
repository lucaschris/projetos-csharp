﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Drawing;
using System.Drawing.Imaging;

using System.Net.Http;
using System.Net;

using System.IO;


namespace XploreAsyncAwait
{
    static class ExtensionStream
    {
        public static MemoryStream ReaderStream(this Stream stream)
        {
            var memoryStream = new MemoryStream();
            int sizeBuffer = 1 << 16;
            byte[] buffer = new byte[sizeBuffer];
            int readed = -1;
            while ( (readed = stream.Read(buffer, 0, buffer.Length) ) > 0)
            {
                memoryStream.Write(buffer, 0, readed);
            }
            return memoryStream;
        }
    }

    static class ExtensionBitmap
    {
        public static string OverToString(this Bitmap bitmap)
        {
            return $"Dim({bitmap.Width}, {bitmap.Height})";
        }

        /**
         * Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
         * Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
         */
        public static void SaveFile(this Bitmap bitmap, string path, string file)
        {
            bitmap.Save($"{path}\\{file}");
        }

        public static void SaveFile(this Bitmap bitmap, string path, string file, ImageFormat imageFormat)
        {
            bitmap.Save($"{path}\\{file}", imageFormat);
        }
    }

    class AsyncDownloadImage
    {

        HttpClient httpClient = new HttpClient();
        /**
         * System.Drawing.Imaging.ImageFormat.Png;
         */
        public async Task DownloadBitmap()
        {
            // http://wallpaperswide.com
            string[] urls = {
                 "http://wallpaperswide.com/download/medusa_4-wallpaper-5120x3200.jpg"
                ,"http://wallpaperswide.com/download/spiderman_in_iran_by_amir_rezaeyan-wallpaper-5120x3200.jpg"
            };
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            foreach (var url in urls)
            {
                Bitmap bitmap = await Download(url);
                Console.WriteLine(bitmap.OverToString());
                ImageFormat imageFormat = bitmap.RawFormat;
                string ext = default(string);
                if(imageFormat.Equals(ImageFormat.Png))
                {
                    ext = "png";
                }

                else if (imageFormat.Equals(ImageFormat.Jpeg))
                {
                    ext = "jpeg";
                }

                bitmap.SaveFile(path, $"{Guid.NewGuid().ToString()}.{ext}", imageFormat);
            }
            return;
        }

        public async Task<Bitmap> Download(string url)
        {
            Bitmap bitmap = default(Bitmap);
            Task<HttpResponseMessage> taskHttpResponse = httpClient.GetAsync(new Uri(url));
            HttpResponseMessage response = taskHttpResponse.GetAwaiter().GetResult();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var taskStream = await response.Content.ReadAsStreamAsync();
                var memoryStream = taskStream.ReaderStream();
                memoryStream.Seek(0, SeekOrigin.Begin);
                bitmap = new Bitmap(memoryStream);
            }
            return bitmap;
        }

        public static async void Run()
        {
            await new AsyncDownloadImage().DownloadBitmap();
        }

        public static void T(string [] args)
        {
            Run();
            Console.WriteLine("END");
        }
    }
}
