﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XploreLinq
{
    /**
     * https://docs.microsoft.com/pt-br/dotnet/csharp/language-reference/keywords/enum
     * 
     * Adicionando metodos a enum com Extesion Methods
     * https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/how-to-create-a-new-method-for-an-enumeration
     * https://stackoverflow.com/questions/5985661/methods-inside-enum-in-c-sharp
     * 
     */
    public enum Office
    {
        Software_Engineer, Mechanical_Engineer, Chemical_Engineer
    };

    /**
     * Extension Method
     */
    static class OfficeExtesion
    {
        // Extesion Methods so podem ser implementados em classes
        // nao aninhadas e o metodo deve ser estático

        public static string GetOfficeName(this Office that)
        {
            string name = "";
            switch (that)
            {
                case Office.Chemical_Engineer:
                    name = "Chemical Engineer";
                    break;

                case Office.Software_Engineer:
                    name = "Software Engineer";
                    break;

                case Office.Mechanical_Engineer:
                    name = "Mechanical Engineer";
                    break;
            }
            return name;
        }
    }

    class Program
    {
        class Employee
        {
            public string Name { get; private set; }
            public double Salary { get; private set; }
            public Office Office { get; private set; }
            public Employee(string name, double salary, Office office)
            {
                Name    = name;
                Salary  = salary;
                Office  = office;
            }

            public override string ToString()
            {
                return string.Format("Nome {0}, Salário {1}, Cargo {2}"
                    , Name, Salary, Office.GetOfficeName());
            }
        }

        static List<Employee> list = new List<Employee>();

 
        public static void init()
        {
            list.Add(new Employee("Chris", 2000, Office.Chemical_Engineer));
            list.Add(new Employee("Lucas", 1500, Office.Mechanical_Engineer));
            list.Add(new Employee("Eduardo", 1500, Office.Mechanical_Engineer));

            list.Add(new Employee("Jeferson", 3000, Office.Software_Engineer));
            list.Add(new Employee("Amanda", 3000, Office.Software_Engineer));
            list.Add(new Employee("Luana", 4000, Office.Chemical_Engineer));
            list.Add(new Employee("Maiara", 4000, Office.Software_Engineer));
            list.Add(new Employee("Marta", 4000, Office.Mechanical_Engineer));

        }


        static void Main(string[] args)
        {
            init();
            anoun();
        }

        static IEnumerable<Employee> MaxSalary()
        {
            /**
             * Query sintax
             */
            IEnumerable<Employee> Employees =
                    from c in list
                    where c.Salary == list.Max(_ => _.Salary)
                    select c;
            foreach (Employee E in Employees)
            {
                Console.WriteLine(E);
            }
            return Employees;
        }

        static IEnumerable<Employee> MinSalary()
        {
            IEnumerable<Employee> Employees =
                    from c in list
                    where c.Salary == list.Min(_ => _.Salary)
                    select c;
            foreach (Employee E in Employees)
            {
                Console.WriteLine(E);
            }
            return Employees;
        }

        static void anoun()
        {
            var T = from c in list
                    where c.Office.Equals(Office.Chemical_Engineer)
                    // objeto anonimo
                    // As vezes nao precisamos do Objeto com todos os seus
                    // atributos. Para isso temos a sintáxe para criar um objeto
                    // anonimo. Quando usamos um objeto anonimo precisamos recorrer 
                    // a inferência de tipos através da palavra-chave 'var'
                    select new { c.Name, c.Office };
            foreach(var E in T) {
                Console.WriteLine(E);
            }
        }

        static void printCountSoftwareEngineers()
        {
            Console.WriteLine(list.Count(_ => _.Office.Equals(Office.Software_Engineer)));
        }

        static void printSumSalaryEmployees()
        {
            Console.WriteLine( list.Sum(_ => _.Salary));
        }

        public static void filterByOffice()
        {
            /*
            IEnumerable<Employee> f = list.Where((Employee e) 
                => { return e.Office.Equals(Office.Mechanical_Engineer); });
                */
            // simplficando
            IEnumerable<Employee> f = list.Where(e => e.Office.Equals(Office.Chemical_Engineer));
            foreach(Employee e in f)
            {
                Console.WriteLine(e);
            }
        }

        public static void filterBySalary()
        {
            IEnumerable<Employee> filtered = list.Where(_ => _.Salary > 1000);
            foreach (Employee e in filtered)
            {
                Console.WriteLine(e);
            }
        }

    }
}
