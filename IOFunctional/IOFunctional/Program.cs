﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOFunctional
{
    class IO
    {
        //
        static Action<object> PrintLine = (object data) => Console.WriteLine(data);
        // leitura de inteiro
        static Func<int> ReadInt = () => int.Parse(ReadLine());
        // leitura de inteiros
        static Func<char, int[]> ReadInts = (char del) => ReadLineSplit(del).Select(x => int.Parse(x)).ToArray();
        // ReadLineString
        static Func<string> ReadLine = () => Console.ReadLine();
        //
        static Func<char, string[]> ReadLineSplit = (char del) => Console.ReadLine().Split(del);
        //
        static Func<string[], int[]> ToIntArray = (string[] data) => data.Select(x => int.Parse(x)).ToArray();
        // read matrix data
        static Func<char, object[][]> ReadMatrix = (char del) =>
        {
            object[][] matrix = null;
            string str = null;
            for (int l=0; (str = ReadLine()) != null; l++)
            {
                string [] splited = str.Split(del);
                for(int c=0; c<splited.Length; c++)
                {
                    matrix[l][c] = splited[c];
                }
            }
            return matrix;
        };

        static Action ReadEndOfLine = () =>
        {
            string str = null;
            while ((str = ReadLine()) != null)
            {

            }
        };

        static Action<char> ReadSplitEndOfLine = (char del) =>
        {
            string [] str = null;
            while ((str = ReadLineSplit(del)) != null)
            {

            }
        };

        public delegate void Println(string format, params object[] args);
        //public static Println println = delegate (string format, object[] args) { Console.WriteLine(format, args); };
        public static Println println = (string format, object[] args) => Console.WriteLine(format, args);

        public static void Main(string[] args)
        {
            foreach(var i in ToIntArray(new string[] { "1", "2", "3" })) {
                println("{0} {1} {2}", i, 0xff, 123);
            }
        }
    }
}
