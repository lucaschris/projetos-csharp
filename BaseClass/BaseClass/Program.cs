﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/**
 * Construtores em C#
 * https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/instance-constructors
 * 
 */

namespace BaseClass
{
    class Program
    {
        public class Point2D
        {
            public int X { get; private set; }
            public int Y { get; private set; }

            public Point2D(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString()
            {
                return $"({X}, {Y})";
            }
        }

        public abstract class Shape2D
        {
            public Point2D S { get; private set; }
            public Point2D T { get; private set; }
            public Shape2D(Point2D s, Point2D t)
            {
                S = s;
                T = t;
            }
            public override string ToString()
            {
                return $"Ponto A { S } - Ponto B { T }";
            }
        }

        public class Square2D : Shape2D
        {
            public Square2D(Point2D s, Point2D t) : base(s, t)
            {
              
            }

            public override string ToString()
            {
                return base.ToString(); // "override";
            }
        }

        /**
         * Sintaxe que aparentemente tem o mesmo resultado
         * quando <? extends Base> no Java
         * https://stackoverflow.com/questions/4732494/cs-equivalent-of-javas-extends-base-in-generics
         * https://msdn.microsoft.com/en-us/library/ms379564(v=vs.80).aspx#csharp_generics_topic4
         */

        public class Data<T> where T : Shape2D
        {
            public int SizeT { get; private set; }
            public int IDX { get; private set; }

            private T[] collection;
            public T this[int i]
            {
                get
                {
                    return collection[i];
                }
                set
                {
                    collection[i] = value;
                }
            }

            public Data(int sizeT)
            {
                SizeT = sizeT;
                collection = new T[sizeT];
            }

            public void print()
            {
                int i = 0;
                do
                {
                    Console.WriteLine(collection[i++]);
                } while (i < SizeT);
                Console.ReadKey();
            }

        }

        static void Main(string[] args)
        {
            Data<Square2D> data = new Data<Square2D>(10);
            for(int i=0; i<data.SizeT; i++)
            {
                data[i] = new Square2D(new Point2D(1,2), new Point2D(3,4));
            }
            data.print();
        }
    }
}
