﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XploreDelegate
{

    delegate Point2D AddPoint(Point2D A, Point2D B);
    
    public class Point2D
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Point2D(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point2D AddPoint2D(Point2D A, Point2D B)
        {
            return new Point2D(A.X + B.X, A.Y + B.Y);
        }

        public override string ToString()
        {
            return $"P({X},{Y})";
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            AddPoint add1 = new AddPoint(Point2D.AddPoint2D);
            Console.WriteLine(add1(new Point2D(1, 2), new Point2D(3, 4)));

            AddPoint add2 = Point2D.AddPoint2D;
            Console.WriteLine(add2(new Point2D(4, 17), new Point2D(3, 4)));


            AddPoint add3 = delegate(Point2D A, Point2D B)
            {

            };

            AddPoint add4 = S =>
            {

            };
        }
    }
}
