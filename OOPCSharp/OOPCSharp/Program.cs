﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPCSharp
{
    class Program
    {

        // http://www.comp.nus.edu.sg/~stevenha/myteaching/competitive_programming/cp1.pdf
        class Point2D
        {
            private int x, y;
            public int X
            {
                get { return x; }
                set { x = value; }
            }

            public int Y
            {
                get { return y; }
                set { y = value; }
            }

            public override string ToString()
            {
                // https://msdn.microsoft.com/pt-br/library/system.string.format(v=vs.110).aspx
                return String.Format("P({0}, {1})", x, y);
                
            }

            public Point2D add(Point2D that)
            {
                Point2D c = new Point2D();
                c.X = x + that.x;
                c.Y = y + that.y;
                return c;
            }
        }

        class Line2D
        {
            Point2D a, b;
        }

        class IO
        {
            public static int ReadInt()
            {
                return int.Parse(Console.ReadLine());
            }

            public static void Write(string str)
            {
                Console.WriteLine(str);
            }

            public static void readKey()
            {
                Console.ReadKey();
            }
        }

        static void Main(string[] args)
        {
            Point2D p1 = new Point2D();
            p1.X = 10;
            p1.Y = 15;
            Point2D p2 = new Point2D();
            p2.X = -15;
            p2.Y = -5;
            Console.WriteLine(p1);
            Console.WriteLine(p1.add(p2));
            Console.ReadKey();
        }
    }
}
