﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OverloadableOp.Program;

/**
 * Sobrecarga de operadores
 * https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/overloadable-operators
 * 
 * Como sobrecarregar indexadores ?
 * http://www.sanfoundry.com/csharp-program-overload-indexers/
 * 
 * Interessante
 * https://stackoverflow.com/questions/287928/how-do-i-overload-the-square-bracket-operator-in-c
 * https://www.tutorialspoint.com/csharp/csharp_indexers.htm
 * https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/indexers/
 * 
 * Porque que para sobrecarregar um operador os metodos devem ser estaticos em c#
 * https://blogs.msdn.microsoft.com/ericlippert/2007/05/14/why-are-overloaded-operators-always-static-in-c/
 * 
 */

namespace OverloadableOp
{

    class Program
    {

        public class Point2D
        {
            public int X { get; private set; }
            public int Y { get; private set; }

            public Point2D(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString() => $"P({X}, {Y})";

            public static Point2D operator +(Point2D a, Point2D b)
            {
                return new Point2D(a.X + b.X, a.Y + b.Y);
            }

            public static Point2D operator -(Point2D a, Point2D b)
            {
                return new Point2D(a.X - b.X, a.Y - b.Y);
            }

            public static bool operator < (Point2D a, Point2D b)
            {
                return a.X < b.X || a.Y < b.Y;
            }

            public static bool operator > (Point2D a, Point2D b)
            {
                return ! (a < b); 
            }
        }


        static void xplorerOverloadOp()
        {
            Point2D a = new Point2D(1, 2);
            Point2D b = new Point2D(10, 12);

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a < b);
            Console.WriteLine(a > b);
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            xploreOverrideIndexers();
        }

        public static void xploreDefaultKeyword()
        {
            // default keyword
            // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/default
            Console.WriteLine($"default(int): {default(int)}");
            Console.WriteLine($"default(double): {default(double)}");
            Console.WriteLine($"default(float): {default(float)}");
            Console.WriteLine($"default(bool): {default(bool)}");
            Console.WriteLine($"default(decimal): {default(decimal)}");
            Console.WriteLine($"default(Int32): {default(Int32)}");
            // tambem tem o uso no Switch-Case(s)
            Console.ReadKey();
        }

        /**
         * Exemplo de sobrecarga de indexadores em c#
         */

        public class Pair<K, V>
        {
            public K Key { get; set; }
            public V Value{ get; set; }
            public Pair(K k, V v)
            {
                Value = v;
                Key = k;
            }

            public override string ToString()
            {
                return $"Key: {Key}, Value: {Value}";
            }

            public override bool Equals(object obj)
            {
                Pair<K, V> that = (Pair<K, V>)obj;
                return that.Key.Equals(Key) && that.Value.Equals(Value);
            }
        }

        public class Collection<K, V>
        {
            private Pair<K, V>[] collection;

            public Collection(int s)
            {
                Size = s;
                collection = new Pair<K, V>[s];
            }

            public int Size { get; set; }
            public Pair<K, V> this[int i]
            {
               get
                {
                    return i >= Size || i < 0 ? null : collection[i];
                }
               set { collection[i] = value; }
            }

            /**
             * Default keyword in generic code
             * https://msdn.microsoft.com/en-us/library/xwth0h0d(v=vs.80).aspx
             * http://www.sanfoundry.com/csharp-program-overload-indexers/
             * https://blogs.msdn.microsoft.com/ericlippert/2007/05/14/why-are-overloaded-operators-always-static-in-c/
             */
            public int this[Pair<K, V> pair]
            {
                get
                {
                    int i = -1;
                    for (int idx = 0; idx < Size; idx++)
                    {
                        if (collection[idx].Equals(pair))
                        {
                            i = idx;
                            break;
                        }
                    }
                    return i;
                }
                private set { }
            }

            // old indexer
            public Pair<K, V> find(Pair<K, V> pair)
            {
                Pair<K, V> p = default(Pair<K, V>);
                for (int idx = 0; idx < Size; idx++)
                {
                    if (collection[idx].Equals(pair))
                    {
                        p = collection[idx];
                        break;
                    }
                }
                return p;
            }

            public override string ToString()
            {
                return base.ToString();
            }
        }

        public static void xploreOverrideIndexers()
        {
            Pair<int, string> pair0 = new Pair<int, string>(0, "A");
            Pair<int, string> pair1 = new Pair<int, string>(15, "ABC");
            Pair<int, string> pair2 = new Pair<int, string>(3, "XYZ");
            Collection<int, string> c = new Collection<int, string>(3);
            c[0] = pair0;
            c[1] = pair1;
            c[2] = pair2;
            Console.WriteLine(c[1]);
            Console.WriteLine(c[2] == null);
            Console.WriteLine(c[3] == null);
            Console.WriteLine(c[c[pair2]] == null);
            Console.ReadKey();
        }
    }
}
