﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * https://docs.microsoft.com/pt-br/dotnet/csharp/language-reference/keywords/override
 */

namespace XploreKeywordOverride
{

    abstract class Form2D
    {

        private int w, h;


        /**
         * Documentacao sobre properties
         * https://docs.microsoft.com/pt-br/dotnet/csharp/programming-guide/classes-and-structs/properties
         */
        public int W
        {
            get { return w; }
            set { w = value; }
        }

        public int H
        {
            get { return h; }
            set { h = value; }
        }

        public int Area()
        {
            return w * h;
        }

        public override string ToString()
        {
            return String.Format("2D: w{0} h{1}", w, h);
        }
    }

    abstract class Form3D: Form2D
    {
        private int d;
        public int D
        {
            get { return d; }
            set { d = value; }
        }

        public override string ToString()
        {
            return String.Format("3D: w{0} h{1} d{2}", W, H, d);
        }
    }

    class Cube : Form3D
    {
        public Cube(int w, int h, int d)
        {
            this.W = w;
            this.H = h;
            this.D = d;
        }

        public int area()
        {
            return W * H * D;
        }

        public override string ToString()
        {
            return String.Format("Cube: w:{0} h:{1} d:{2}", W, H, D);
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Cube cube = new Cube(10,20,25);
            Console.WriteLine(cube);

            Console.WriteLine($"Width {cube.W}");

            Console.ReadKey();
        }
    }
}
