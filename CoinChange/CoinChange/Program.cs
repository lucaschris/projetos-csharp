﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoinChange
{
    class Program
    {

        public static int topDown(int goal, int [] values, int ith)
        {
            if (goal < 0)
                return 0;   // nao tem solucao
            if(goal == 0)
            {
                return 1;   // existe uma solucao
            }

            if (ith == values.Length)
                return 0;   // nao tem solucao

            int a = topDown(goal - values[ith], values, ith);       // solucao adicionando o ith valor
            int b = topDown(goal, values, ith+1);                   // solucao sem adicionar o ith valor
            return a + b;
        }

        /**
         * http://www.ccs.neu.edu/home/jaa/CS7800.12F/Information/Handouts/dyn_prog.pdf
         * http://www.ccs.neu.edu/home/jaa/CSG713.04F/Information/Handouts/dyn_prog.pdf
         * http://www.columbia.edu/~cs2035/courses/csor4231.F07/dynamic.pdf
         */
        public static int solver2(int goal, int [] values)
        {
            int[] C = new int[goal];
            int[] S = new int[goal];
            for(int p=0; p<goal; p++)
            {
                int m = int.MaxValue;
                int c = -1;
                for(int q=0; q<values.Length; q++)
                {
                    if(values[q] <= goal)
                    {
                        if( 1 + C[goal - values[q]] < m)
                        {
                            m = 1 + C[goal - values[q]];
                            c = q;
                        }
                    }
                }
                C[p] = m;
                S[p] = c;
            }
            return S[goal-1];
        } 

        public static int bottomUp(int goal, int [] values)
        {
            return 0;
        }


        public static void test1()
        {
            int[] values = { 1, 2, 3 };
            Console.WriteLine(topDown(5, values, 0));
        }

        public static void test2()
        {
            int[] values = { 1, 2, 3 };
            Console.WriteLine(solver2(5, values));
        }

        static void Main(string[] args)
        {
            test2();
        }
    }
}
