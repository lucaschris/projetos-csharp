﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Diferenca de Properties e Indexers
 * https://docs.microsoft.com/pt-br/dotnet/csharp/programming-guide/indexers/comparison-between-properties-and-indexers
 * https://docs.microsoft.com/pt-br/dotnet/csharp/programming-guide/indexers/index
 * https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/indexers/using-indexers
 * Indexers
 * 
 * Os indexadores permitem encapsular o acesso a atributos de classes que representam uma coleção de dados (Arrays).
 * Eles fornecem através das keywords get e set uma forma de acessar e modificar os dados de uma colecao, de forma
 * similar a outro recurso do c#, os properties. Indexers e Properties possuem características em comum bem como
 * diferenças, listo-as abaixo:
 * 
 * Indexadores devem ser membros de instância, diferente de properties que podem ser membros de classe (variáveis estáticas)
 * 
 * Indexadores não possuem sintáxe reduzida como Propriedades que podem ser criadas só com get e set
 * 
 * Indexadores permitem que objetos sejam acessados usando a mesma sintaxe de matriz ColecaoDeObjetos[indice]
 * A palavra chave this serve para definir um indexador
 */

namespace XploreIndexers
{
    class Program
    {
        public class ArrayData<T>
        {
            //static private T[] data;
            private T[] data;
            // Property. Auto-implemented Property
            public int SizeT { get; private set; }
            // uma propriedade pode ser estatica
            //public static int D { get; set; }

            // Indexer: Deve ser um membro de instância
            public T this[int i]
            {
                get { return data[i]; }
                set { data[i] = value; }
            }

            public ArrayData(int sizeT)
            {
                SizeT = sizeT;
                data = new T[sizeT];
            }

            public void Print()
            {
                int i = 0;
                do
                {
                    Console.WriteLine(data[i++]);
                } while (i < SizeT);
            }
        }

        public class Data<T>
        {
            private T[] collection;
            public int Size { get; private set; }

            private int idx;

            public int IDX { get => idx; set => idx = value; }

            // indexador, somente leitura. A partir do c# 6
            //public T this[int i] => collection[i];

            // c#7
            public T this[int i]
            {
                get => collection[i];
                set => collection[i] = value;
            }

            public Data(int size)
            {
                Size = size;
                IDX  = 0;
                collection = new T[size];
            }

            public void Add(T data)
            {
                collection[(IDX)++] = data;
            }

            public void Print()
            {
                int idx = 0;
                do
                {
                    Console.WriteLine(this[idx++]);
                } while (idx < Size);
            }
        }

        static void sample1()
        {
            ArrayData<int> arrayData = new ArrayData<int>(10);
            for (int i = 0; i < arrayData.SizeT; i++)
            {
                arrayData[i] = 0xff << i;
            }
            arrayData.Print();
            Console.ReadKey();
        }

        static void sample2()
        {
            Data<int> arrayData = new Data<int>(10);
            for (int i = 0; i < arrayData.Size; i++)
            {
                //arrayData.Add(0xf << i);
                // a linha abaixo funciona por causa da declaracao de Proprieda
                arrayData[i] = 0xf << i;
            }

            arrayData.Print();
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            sample2();

        }
    }
}
