﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * https://www.hackerrank.com/challenges/ctci-balanced-brackets/problem
 * DONE
 */

namespace Stack
{
    class Program
    {

        class IO
        {
            public static int ReadInt()
            {
               return int.Parse(Console.ReadLine());
            }

            public static void WriteFormat(string format, params object[] data)
            {
                Console.Write(format, data);
            }

            public static void WriteLine(string format, params object[] data)
            {
                Console.WriteLine(format, data);
            }

            /**
             * varags do c#
             * https://stackoverflow.com/questions/18194849/is-there-a-c-sharp-alternative-to-javas-vararg-parameters
             */
            public static void WriteLine(params object[] data)
            {
                Console.WriteLine(data);
            }
        }

        static void Main(string[] args)
        {
            int q = IO.ReadInt();
            while(q>0)
            {
                string line = Console.ReadLine();
                bool flag = true;
                Stack<char> stack = new Stack<char>();
                foreach (char c in line)
                {
                    if (c == '(' || c == '[' || c == '{')
                        stack.Push(c);
                    else
                    {
                        bool f = stack.Count > 0 && (
                            (c == ')' && stack.Peek() == '(')
                            || (c == ']' && stack.Peek() == '[')
                            || (c == '}' && stack.Peek() == '{')
                            );
                      
                        if (f)
                        {
                            stack.Pop();
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                IO.WriteLine(flag && stack.Count == 0 ? "YES" : "NO");
                q--;
            }
            //Console.ReadKey();
        }
    }
}
